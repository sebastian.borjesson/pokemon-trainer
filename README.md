<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">Pokemon Trainer</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#app-overview">App Overview</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.

Link to demo: [pokemon-trainer](...heroku-url)

## Built with

- AngularJS
- Bootstrap

## Getting Started

### Prerequisites

[VSCode](https://code.visualstudio.com/) or any other editor.

[Angular-CLI](https://angular.io/cli)


### Installation

1. Clone the repo
    ```sh
    git clone https://gitlab.com/sebastian.borjesson/pokemon-trainer.git
    ```
2. Enter the project folder
    ```sh
    cd pokemon-trainer
    ```
3. Install npm packages
    ```sh
    npm install
    ```
4. Run the app
    ```sh
    ng serve
    ```

<p align="right">(<a href="#top">back to top</a>)</p>

## App Overview

AuthGuard sets boolean isLoggedIn if there is a username in sessionStorage and redirects to catalogue. If you return to "/" route the sessionStorage is cleared. Regex restricts input to letters. 

After login/registration there is a fetch for 151 pokemon (OG generation) and a subsequent fetch for ever individual pokemon based on the names in the original fetch which is stored in sessionStorage, and adds isCollected boolean to every object and sets to false. The catch button patches an updated array to noroff-api and also sets the isCollected boolean to true which dynamically changes the button to a poke-ball. 

The trainer page loads all pokemon from sessionStorage and filters out every object with isCollected=false. If your collection is empty it shows a message instead.

The navbars log out button redirects to login page which in effect logs you out since it clears sessionStorage. The links aren't shown on the login page and respectively turns red when active.

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

[Nils Jacobsen](https://gitlab.com/nils_jacobsen)

[Sebastian Börjesson](https://gitlab.com/sebastian.borjesson)
