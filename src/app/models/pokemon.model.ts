export interface Pokemon {
    id: string,
    name: string,
    image?: string,
    isCollected: boolean
}  

