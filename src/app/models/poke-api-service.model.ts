export interface PokeAPIResponse {
    count: number,
    next: string,
    results: PokeAPIResults[] // result = {name, url} - use name when fetching details for one pokemon
}
  
export interface PokeAPIResults {
    name: string,
    url: string
}

export interface PokeAPIPokemon {
    id: string,
    name: string,
    sprites: Sprites
}

export interface Sprites {
front_default?: string
}
  