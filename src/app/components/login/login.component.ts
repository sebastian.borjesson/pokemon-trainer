import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  
  constructor(
    private readonly userService: UserService,
    private router: Router
    ) { }
    
    ngOnInit(): void {
      this.userService.fetchUsers()
      localStorage.clear();     
      sessionStorage.clear();   //All storage will be cleared when user clicks the log out button.
    }
    
    /**
     * Function for handling userlogin
     * If the user exists in the Noroff-API, login that user
     * If not, register and login the new user
     */

    public onSubmit(submitLogin: NgForm) {
      const userArray = this.userService.users();
      const username = submitLogin.value.username;
      let match = false;
      for (let index = 0; index < userArray.length; index++) {
        if(userArray[index].username === username) {
          match = true
        }
      }
      if(match === false){
        const temp: User = {
          id: 0,
          username: submitLogin.value.username,
          pokemon: []
        }

        this.userService.registerUser(temp).subscribe({
          next: data => {
            localStorage.setItem("username", data.username)
            this.router.navigate(['/catalogue'])
          }
        });
        
      } else {
        localStorage.setItem("username", username)
        this.router.navigate(['/catalogue'])
      }
  }

}
