import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../models/pokemon.model';
import { PokeApiService } from '../../services/poke-api.service';
import { UserService } from '../../services/user.service';
import { TrainerService } from '../../services/trainer.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  private _APIPokemon: Pokemon[] = [];
  private _userPokemon?: string[] = [];

  constructor(
    private readonly pokeApiService: PokeApiService, 
    private readonly userService: UserService, 
    private readonly trainerService: TrainerService
  ) { }
    
  ngOnInit(): void {
    this.pokeApiService.getAPIPokemon();
    this.userService.fetchCurrentUser();
  }

  get pokemonList(): Pokemon[] {
    this._APIPokemon = this.pokeApiService.pokemon();
    this._userPokemon = this.userService.getUserPokemons();
    this._APIPokemon.sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id ? -1 : 0)));
    /**
      * If the pokemon is collected by the user,
      * Set the pokemon in the catalogue as collected
      * and hide the catch button
     */
    for (let index = 0; index < this._APIPokemon.length; index++) {
      if(this._userPokemon.includes(this._APIPokemon[index].name)){
        this._APIPokemon[index].isCollected = true;
      }
    }
    return this._APIPokemon;
  }

  /**
    * Function to add pokemon on the user and post to Noroff-API
  */
  public addPokemon(name: string): void{
    this.pokeApiService.catchPokemon(name);
    this.trainerService.addPokemonToTrainer(name);
  }
}