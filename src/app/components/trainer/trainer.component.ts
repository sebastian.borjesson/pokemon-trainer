import { User } from '../../models/user.model';
import { Component, OnInit } from '@angular/core';
import { PokeApiService } from '../../services/poke-api.service';
import { UserService } from '../../services/user.service';
import { TrainerService } from '../../services/trainer.service';
import { Pokemon } from '../../models/pokemon.model';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {

  constructor(
    private readonly pokeApiService: PokeApiService, 
    private readonly userService: UserService,
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
    this.pokeApiService.getAPIPokemon();
    this.userService.fetchCurrentUser();
  }

  get pokemonList(): Pokemon[] {
    return this.pokeApiService.pokemon();
  }

  get currentUser(): User[] {
    return this.userService.currentUser();
  }

  public removePokemon(name: string): void {
    this.pokeApiService.releasePokemon(name)
    this.trainerService.removePokemonFromTrainer(name);
  }

}