import { Pokemon } from '../models/pokemon.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokeAPIResponse, PokeAPIResults, PokeAPIPokemon } from "../models/poke-api-service.model"

/**
 * Interfaces for the API call when fetching pokemon
 */

@Injectable({
  providedIn: 'root'
})


export class PokeApiService {

  private _baseUrl: string = 'https://pokeapi.co/api/v2/pokemon';
  private _pokeAPIResults?: PokeAPIResults[];
  private _pokemon: Pokemon[] = [];
  private _pokeNameMap: Map<string, Pokemon> = new Map();
  
  constructor(private readonly http: HttpClient) { }

  public getAPIPokemon(): void {
    /**
     * If sessionStorage contains pokemon, map them so we can use that instead of making a API call
     */
    if(sessionStorage.getItem('pokemon') !== null){
      const pokeStorage: Pokemon[] = JSON.parse(sessionStorage.getItem('pokemon') || '');
      this._pokemon = pokeStorage;
      for(let i = 0; i < this._pokemon.length; i++){
        this._pokeNameMap.set(this._pokemon[i].name, this._pokemon[i]);
      }
      return;
    }
    this._pokemon = [];
    this.http.get<PokeAPIResponse>(this._baseUrl+"?limit=151").subscribe({
      next: (response: PokeAPIResponse) => {
        // Each response from the api we store in a array
        this._pokeAPIResults = response.results;
        // Loop through the reponse and collect data for each pokemon
        this._pokeAPIResults.forEach((pokemonName: {name: string}) => {
          this.http.get<PokeAPIPokemon>(`https://pokeapi.co/api/v2/pokemon/${pokemonName.name}`).subscribe({
            next: (response) => {
              // Deconstruct the object we get back with the properties that are necessary
              const { id, name, sprites } = response;
              const image = sprites.front_default;
              // Create a new instance of our pokemon model and append it to the pokemon array
              const pokemon: Pokemon = {id, name, image, isCollected: false};
              this._pokemon.push(pokemon);
              sessionStorage.setItem("pokemon",JSON.stringify(this._pokemon));
            }
          })
        })
      }
    })
  }
  
  public catchPokemon(name: string) {
    /**
     * Loop through the pokemon array
     * Set the pokemon name the user has added to collected
     */
    for (let index = 0; index < this._pokemon.length; index++) {
      if(this._pokemon[index].name === name){
        this._pokemon[index].isCollected = true;
      }
    }
    sessionStorage.setItem('pokemon', JSON.stringify(this._pokemon));
  }

  public releasePokemon(name: string) {
    /**
     * Loop through the pokemon array
     * Remove the isCollected tag on the pokemon
     * This makes sure that you can catch it again
     */
    for (let index = 0; index < this._pokemon.length; index++) {
      if(this._pokemon[index].name === name){
        this._pokemon[index].isCollected = false;
      }
    }
    sessionStorage.setItem('pokemon', JSON.stringify(this._pokemon));
  }
  

  // Getters
  public pokemon(): Pokemon[] {
    return this._pokemon;
  }

}

