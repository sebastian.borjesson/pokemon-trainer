import { User } from './../models/user.model';
import { UserService } from './user.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TrainerService {

   
  private _trainerApiUrl: string = 'https://sebastian-dce-noroff-api.herokuapp.com/trainers';
  private _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'X-API-KEY': 'zwymVdtSxUWq6L2NbM6rsA=='
    })
  };

  constructor(private readonly http: HttpClient, private readonly userService: UserService) { }


  public addPokemonToTrainer(name: string): void {
    const user = this.userService.currentUser();
    user[0].pokemon.push(name);
    this.http.patch<User[]>(this._trainerApiUrl+'/'+user[0].id, user[0], this._httpOptions)
    .subscribe(
      (user: User[]) => {
      
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
  }

  public removePokemonFromTrainer(name: string): void {
    const currentUser = this.userService.currentUser();
    const pokemonArray = currentUser[0].pokemon;
    // Find the index of the pokemon to remove
    const pokemonName = pokemonArray.indexOf(name);
    // If the index is bigger than -1, splice the array and remove the index from the array
    if (pokemonName > -1) {
      pokemonArray.splice(pokemonName, 1); // remove one item only
    }
    // Update the user on Noroff-API
    
    this.http.patch<User[]>(this._trainerApiUrl+'/'+currentUser[0].id, currentUser[0], this._httpOptions)
    .subscribe(
      (user: User[]) => {

      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
  }

}
