import { User } from './../models/user.model';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _users: User[] = []
  private _currentUser: User[] = []
  private _userPokemon: string[] = [];
   
  private _trainerApiUrl: string = 'https://sebastian-dce-noroff-api.herokuapp.com/trainers';
  private _httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'X-API-KEY': 'zwymVdtSxUWq6L2NbM6rsA=='
    })
  };

  constructor(private readonly http: HttpClient) { }

  // API calls
  public fetchUsers(): void {
    this.http.get<User[]>(this._trainerApiUrl)
    .subscribe(
      (users: User[]) => {
      this._users = users;
      }, 
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
  }

  public fetchCurrentUser(): void {
    this.http.get<User[]>(this._trainerApiUrl+`?username=${localStorage.getItem('username')}`)
    .subscribe(
      (user: User[]) => {
        this._currentUser = user;
        this._userPokemon = this._currentUser[0].pokemon;
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    )
  }

  public registerUser(user: User): Observable<User> {
    return this.http.post<User>(this._trainerApiUrl, user, this._httpOptions)
  }

  // Getters
  public users(): User[] {
    return this._users;
  }

  public currentUser(): User[] {
    return this._currentUser;
  }

  public getUserPokemons() {
    return this._userPokemon;
  }

}
